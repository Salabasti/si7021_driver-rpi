obj-m := si7021.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	dtc -@ -Hepapr -I dts -O dtb -o si7021.dtbo si7021_overlay.dts

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm si7021.dtbo

install:
	dtoverlay si7021.dtbo
	insmod si7021.ko

uninstall:
	dtoverlay -r si7021
	rmmod si7021
