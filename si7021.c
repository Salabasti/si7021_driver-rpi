
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>


#define CMD_HUM 0xE5
#define CMD_TEM 0xE3
#define I2C_ERR -100000

#define BUFFER_SIZE 50


// ===================== module info =====================

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sebastian Helmer");
MODULE_DESCRIPTION("Driver to read Values from Si7021 Sensor");
MODULE_VERSION("0.01");


// ===================== driver definition =====================

// ------------------ client handler ------------------------
static struct i2c_client *si7021_client;

/* As soon as the overlay was loaded this function gets called.
It provieds an i2c_client structure which is saved and used for
further communication.
--------------------- probe callback -----------------------*/
static int probe(struct i2c_client *client, const struct i2c_device_id *id) {
	printk(KERN_INFO "DEBUG Si7021: probe\n");
	si7021_client = client;

	return 0;
}


// The following strucures are used to define which devices should be
// compatible with this driver ...

static struct i2c_device_id idtable[] = {
	{ "si7021", 1 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, idtable);

static struct i2c_driver si7021 = {
	.driver = {
		.name	= "si7012",
	},

	.id_table	= idtable,
	.probe		= probe,
};


// ==================== read sensor data =======================

/*
Here the actual reading of the sensor is done. First a command-byte is send,
which determines is hum or tmp is read.
Next two byte are read which contain the data. Because floats are not
supported here, the result gets returned multiplied by 100.
----------------------- read temperature -------------------*/
static int read_value(const char command) {
	int return_val;
	char buffer[2];
	int value = 0;

	if (!si7021_client)
		return I2C_ERR;

	return_val = i2c_master_send(si7021_client, &command, 1);
	if (return_val < 0 ) {
		printk(KERN_INFO "DEBUG Si7021: error writing so sensor\n");
		return I2C_ERR;
	}

	mdelay(300);

	return_val = i2c_master_recv(si7021_client, buffer, 2);
	if (return_val < 0) {
		printk(KERN_INFO "DEBUG Si7021: error reading sensor\n");
		return I2C_ERR;
	}

	value = (buffer[0] << 8) + buffer[1];
	if (command == CMD_HUM) {
		value = ((12500 * value) / 65536) - 600;
	} else if (command == CMD_TEM) {
		value = (value * 17572) / 65536 - 4685;
	}

	return value;
};


// ===================== device file =====================

/*
This functions gets called each time the device file, located in /proc is read.
It will read out the sensor and write the values in a user-buffer, encoded as
a JSON object. Therefore the values will be encoded with two decimal points.
--------------------- read device file -----------------------*/
static ssize_t device_read(struct file *file, char *user_buffer, size_t count, loff_t *offset) {
	char buffer[BUFFER_SIZE];
	int len = 0;
	int hum, hum_upper, hum_lower;
	int tmp, tmp_upper, tmp_lower;

	hum = read_value(CMD_HUM);
	tmp = read_value(CMD_TEM);

	hum_upper = hum / 100;
	hum_lower = hum - hum_upper * 100;
	tmp_upper = tmp / 100;
	tmp_lower = tmp - tmp_upper * 100;

	if(*offset > 0 || count < BUFFER_SIZE)
		return 0;

	len += sprintf(buffer, "{\n\t\"humidity\": %d.%d,\n", hum_upper, hum_lower);
	len += sprintf(buffer + len, "\t\"temperature\": %d.%d\n}\n", tmp_upper, tmp_lower);

	if(raw_copy_to_user(user_buffer, buffer, len))
		return -EFAULT;

	*offset = len;
	return len;
}


static struct proc_dir_entry *entry;

// --------------------- file ops -----------------------
static struct file_operations file_ops = {
	.owner = THIS_MODULE,
 	.read = device_read,
};


// ===================== driver lifecycle =====================

// --------------------- init -----------------------
static int my_init(void) {
	printk(KERN_INFO "DEBUG Si7021: init\n");

	i2c_add_driver(&si7021);

	entry = proc_create("si7021", 0660, NULL, &file_ops);

  	return  0;
}


// --------------------- exit -----------------------
static void my_exit(void) {
	printk(KERN_INFO "DEBUG Si7021: exit\n");

  i2c_del_driver(&si7021);

	proc_remove(entry);

  return;
}


module_init(my_init);
module_exit(my_exit);
