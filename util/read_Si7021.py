import smbus
import time

def readSensor():
    i2cBus = smbus.SMBus(1)
    try:
        i2cBus.write_byte(0x40, 0xE5)
        time.sleep(0.3)
        data_top = i2cBus.read_byte(0x40)
        data_low = i2cBus.read_byte(0x40)
        humidity = (((((data_top & 0xFF) * 256) + (data_low & 0xFF)) * 125.0) / 65536.0) - 6

        i2cBus.write_byte(0x40, 0xE3)
        time.sleep(0.02)
        data_top = i2cBus.read_byte(0x40)
        data_low = i2cBus.read_byte(0x40)

        temperature = (((((data_top & 0xFF) * 256.0) + (data_low & 0xFF)) * 175.72) / 65536.0) - 46.85
    except:
        print("ERROR READING SENSOR ON SOCKET!")
        humidity = -1
        temperature = -1

    return temperature, humidity

# ------------------------------------------------------------------------------

print("reading sensor ...")
tmp, hum = readSensor()
print("Temperature: " + str(tmp) + "; Humidity: " + str(hum))
