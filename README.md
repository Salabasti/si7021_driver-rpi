# Si7021 Temperature / Humidity Sensor i2c driver (kernel module)

This repository contain a simple implementation for reading out the temperature and humidity of a si7021 sensor. The driver is implemented as an linux kernel module and uses i2c to communicate with the device. 

*This driver was designed for raspberry pi only. Thus other systems were not tested and might need adjustments.*

## Prerequisites

To build the kernel module linux kernel headers must be installed. On raspberry pi this can be done via:

`sudo apt install raspberrypi-kernel-headers`

## compile and load the module

All code for the kernel module is contained in *si7021.c*. To enable communication from the driver to the sensor an additional device tree entry is needed. For this a device tree overlay was defined in *si7021_overlay.dts*.

To build the module and the overlay just run: 
`make`

Loadng the kernel module can then be done via:
`sudo insmod si7021.ko`

To enable i2c communication in the driver the overlay sould be loaded with:
`sudo dtoverlay si7021.dtbo `

Also `sudo make install` can be used to install the module and the overlay. The other way `sudo make uninstall` can be used to revert installation. 

## reading sensor data

The driver provides a simple device file at the path */proc/si7021*, which can be used to read out the sensor values. The result is encoded as a JSON object. Accuracy is limited to two decimal units in this implementation. 

An example for reading out the values can look as follows: `sudo cat /proc/si7021`, with the result:
```
{
        "humidity": 51.55
        "temperature": 23.19
}
```

